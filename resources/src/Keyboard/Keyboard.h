/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__

#include <QLabel>
#include "../mecanique/Object.h"


/**
\brief	Classe de capture des entrées clavier.
*/
class InputKeyboard : public QWidget
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	
	\param	Parent	Widget parent.
	*/
	InputKeyboard(QWidget *Parent=0);
	///Destructeur.
	~InputKeyboard();
	
	signals:
	///Est émis lors de l'appui d'une touche.
	void keyPressed(int Key);
	
	private:
	///Émet « keyPressed() ».
	void keyPressEvent(QKeyEvent *Event);
	
};

/**
\brief	Classe de représentation d'objet clavier.
*/
class Keyboard : public Object
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	
	\param	Name	Nom du l'objet.
	\param	Project	Projet de rattachement.
	*/
	Keyboard(QString Name, Project* const Project);
	///Destructeur.
	~Keyboard();
	
	///Ouvre la fenêtre de capture des entrées clavier.
	void more();
	
	///Retourne un QVariant vide.
	QVariant settings();
	///Ne fait rien.
	void setSettings(QVariant Settings);
	
	public slots:
	///Émet le signal correspondant à la touche spécifiée.
	virtual void keyPressed(int Key) = 0;
	
	private:
	///Fenêtre de capture des entrées clavier.
	InputKeyboard *dialogInput;
};

#endif /* __KEYBOARD_H__ */
