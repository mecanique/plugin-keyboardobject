/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "Keyboard.h"

InputKeyboard::InputKeyboard(QWidget *Parent) : QWidget(Parent, Qt::Dialog)
{
setWindowTitle(tr("Input keyboard"));

QLabel *tempImgLabel = new QLabel(this);
	tempImgLabel->setPixmap(QPixmap(QCoreApplication::applicationDirPath() + "/../share/icons/types/Keyboard.png"));
QLabel *tempTextLabel = new QLabel(this);
	tempTextLabel->setText("<center>" + tr("Give focus to this dialog box to activate the keyboard input.") + "</center>");
QVBoxLayout *tempLayout = new QVBoxLayout(this);
	tempLayout->addWidget(tempImgLabel, 0, Qt::AlignCenter);
	tempLayout->addWidget(tempTextLabel, 0, Qt::AlignCenter);
}

InputKeyboard::~InputKeyboard()
{
}

void InputKeyboard::keyPressEvent(QKeyEvent *Event)
{
if (!Event->isAutoRepeat())
	{
	emit keyPressed(Event->key());
	Event->accept();
	}
else Event->ignore();
}

Keyboard::Keyboard(QString Name, Project* const Project) : Object(Name, "Keyboard", Project)
{
changeStatus(Object::Operational);
changeInfos("Keyboard active.");
dialogInput = new InputKeyboard(Project);
connect(dialogInput, SIGNAL(keyPressed(int)), SLOT(keyPressed(int)));
}

Keyboard::~Keyboard()
{
}
	
void Keyboard::more() 
{
dialogInput->show();
}
	
QVariant Keyboard::settings()
{
return QVariant();
}

#pragma GCC diagnostic ignored "-Wunused-parameter"
void Keyboard::setSettings(QVariant Settings)
{
return;
}
#pragma GCC diagnostic pop


