/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecKeyboardObjectPlugin.h"

MecKeyboardObjectPlugin::MecKeyboardObjectPlugin() : MecPlugin(QString("keyboardobject"), __KEYBOARDOBJECT_VERSION__, MecAbstractElement::Object, QString("Keyboard"), QString("Keyboard object"))
{
}

MecKeyboardObjectPlugin::~MecKeyboardObjectPlugin()
{
}

QString MecKeyboardObjectPlugin::description() const
{
return QString(tr("Manage the keyboard and its signals."));
}

QString MecKeyboardObjectPlugin::copyright() const
{
return QString("Copyright © 2013 Quentin VIGNAUD");
}

QString MecKeyboardObjectPlugin::developpers() const
{
return QString("Quentin VIGNAUD <>\n");
}

QString MecKeyboardObjectPlugin::documentalists() const
{
return QString("Quentin VIGNAUD <>\n");
}

QString MecKeyboardObjectPlugin::translators() const
{
return QString("");
}

MecAbstractElementEditor* MecKeyboardObjectPlugin::elementEditor(MecAbstractElement* const Element, MecAbstractEditor* MainEditor)
{
if (Element->elementRole() == MecAbstractElement::Object)
	{
	MecKeyboardObjectEditor *tempEditor = new MecKeyboardObjectEditor(static_cast<MecAbstractObject*>(Element), MainEditor);
	tempEditor->childListElementsChanged(Element);
	return tempEditor;
	}
else
	{
	return 0;
	}
}

MecAbstractElementCompiler* MecKeyboardObjectPlugin::elementCompiler(MecAbstractElement* const Element, MecAbstractCompiler* const MainCompiler)
{
if (Element->elementRole() == MecAbstractElement::Object)
	{
	MecKeyboardObjectCompiler *tempCompiler = new MecKeyboardObjectCompiler(static_cast<MecAbstractObject*>(Element), MainCompiler);
	return tempCompiler;
	}
else
	{
	return 0;
	}
}

