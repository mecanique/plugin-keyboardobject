/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECKEYBOARDOBJECTEDITOR_H__
#define __MECKEYBOARDOBJECTEDITOR_H__

#include <MecObjectEditor.h>
#include "MecKeyboardSignalEditor.h"

/**
\brief	Classe d'édition d'objet de type « Keyboard ».
*/
class MecKeyboardObjectEditor : public MecObjectEditor
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	\param	Object	Objet édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	*/
	MecKeyboardObjectEditor(MecAbstractObject* const Object, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	~MecKeyboardObjectEditor();
	
	///Indique si une MecFunction peut être ajouté, retourne donc \e false.
	bool canAddFunction() const;
	///Indique si une MecVariable peut être ajouté, retourne donc \e false.
	bool canAddVariable() const;
	
	public slots:
	/**
	\brief	Demande un sous-éditeur pour \e Element.
	\note	\e Element doit être un élément enfant de element().
	\return	Le sous-éditeur demandé, ou 0 si éditer cet élément n'est pas possible.
	*/
	MecAbstractElementEditor* newSubEditor(MecAbstractElement *Element);
	
	/**
	\brief	Ajoute un signal à l'élément.
	
	Demande à l'utilisateur le signal qu'il souhaite ajouter.
	*/
	void addSignal();
	
	///Ne fait rien.
	void addFunction();
	void addVariable();
	
	signals:
	
	
	private:
	
	
};

#endif /* __MECKEYBOARDOBJECTEDITOR_H__ */

