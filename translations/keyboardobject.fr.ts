<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>MecElementEditor</name>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="32"/>
        <source>Type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="40"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="139"/>
        <source>New object</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="140"/>
        <source>Type of the new object:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="144"/>
        <source>New function</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="145"/>
        <source>Return type of the new function:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="149"/>
        <source>New signal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="150"/>
        <source>Type of the new signal:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="154"/>
        <source>New variable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="155"/>
        <source>Type of the new variable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="182"/>
        <source>Add the object “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="192"/>
        <source>Add the function “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="202"/>
        <source>Add the signal “%1”</source>
        <translation type="unfinished">Ajouter le signal « %1 »</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="212"/>
        <source>Add the variable “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="297"/>
        <source>Change name of “%1” to “%2”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecElementEditor.cpp" line="310"/>
        <source>Change type of “%1” to “%2” from “%3”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecKeyboardObjectEditor</name>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecKeyboardObjectEditor.cpp" line="72"/>
        <source>Select signal</source>
        <translation>Sélectionnez le signal</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecKeyboardObjectEditor.cpp" line="72"/>
        <source>Select the signal to append:</source>
        <translation>Sélectionnez le signal à ajouter :</translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecKeyboardObjectEditor.cpp" line="77"/>
        <source>Add the signal “%1”</source>
        <translation>Ajouter le signal « %1 »</translation>
    </message>
</context>
<context>
    <name>MecKeyboardObjectPlugin</name>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecKeyboardObjectPlugin.cpp" line="32"/>
        <source>Manage the keyboard and its signals.</source>
        <translation>Gestion du clavier et de ses signaux.</translation>
    </message>
</context>
<context>
    <name>MecObjectEditor</name>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecObjectEditor.cpp" line="32"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecObjectEditor.cpp" line="42"/>
        <source>Reference</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecObjectEditor.cpp" line="57"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecObjectEditor.cpp" line="67"/>
        <source>Functions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecObjectEditor.cpp" line="77"/>
        <source>Signals</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecObjectEditor.cpp" line="96"/>
        <source>Variables</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecSignalConnectionsList</name>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecSignalEditor.cpp" line="93"/>
        <source>Remove connections with “%1”</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecSignalEditor.cpp" line="108"/>
        <source>Add connection between “%1” and “%2”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MecSignalEditor</name>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecSignalEditor.cpp" line="190"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecSignalEditor.cpp" line="202"/>
        <source>Variables</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecSignalEditor.cpp" line="222"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecSignalEditor.cpp" line="229"/>
        <source>Add connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecSignalEditor.cpp" line="231"/>
        <source>Remove connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecSignalEditor.cpp" line="239"/>
        <source>Connections</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../../src/mecanique-plugins/keyboardobject/MecSignalEditor.cpp" line="364"/>
        <source>Add connection between “%1” and “%2”</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
