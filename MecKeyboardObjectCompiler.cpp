/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecKeyboardObjectCompiler.h"

MecKeyboardObjectCompiler::MecKeyboardObjectCompiler(MecAbstractObject* const Object, MecAbstractCompiler* const MainCompiler) : MecObjectCompiler(Object, MainCompiler)
{

}

MecKeyboardObjectCompiler::~MecKeyboardObjectCompiler()
{
}

QList<QResource*> MecKeyboardObjectCompiler::resources()
{
QList<QResource*> tempList;
tempList.append(new QResource(":/src/Keyboard/Keyboard.h"));
tempList.append(new QResource(":/src/Keyboard/Keyboard.cpp"));
return tempList;
}
	
QString MecKeyboardObjectCompiler::header()
{
QString tempString("/*\n\
© Quentin VIGNAUD, 2013\n\
\n\
Licensed under the EUPL, Version 1.1 only.\n\
You may not use this work except in compliance with the\n\
Licence.\n\
You may obtain a copy of the Licence at:\n\
\n\
http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available\n\
\n\
Unless required by applicable law or agreed to in\n\
writing, software distributed under the Licence is\n\
distributed on an “AS IS” basis,\n\
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either\n\
express or implied.\n\
See the Licence for the specific language governing\n\
permissions and limitations under the Licence.\n\
*/\n\
\n\
#ifndef __" + object()->elementName().toUpper() + "_H__\n\
#define __" + object()->elementName().toUpper() + "_H__\n\
\n\
#include \"Keyboard/Keyboard.h\"\n\
\n\
class " + object()->elementName() + " : public Keyboard\n\
{\n\
	Q_OBJECT\n\
	\n\
	public:\n\
	" + object()->elementName() + "(Project* const Project);\n\
	~" + object()->elementName() + "();\n\
	\n\
	public slots:\n\
	void keyPressed(int Key);\n\
	\n\
	signals:\n");

for (int i=0 ; i < object()->childSignals().size() ; i++)
{
	QChar tempKey = object()->childSignals().at(i)->elementName().at(object()->childSignals().at(i)->elementName().size() - 1);
	tempString += "void Key" + QString(tempKey) + "();\n";
}

tempString += "};\n\
\n\
#endif /* __" + object()->elementName().toUpper() + "_H__ */\n\n";
return tempString;
}

QString MecKeyboardObjectCompiler::source()
{
QString tempString("/*\n\
© Quentin VIGNAUD, 2013\n\
\n\
Licensed under the EUPL, Version 1.1 only.\n\
You may not use this work except in compliance with the\n\
Licence.\n\
You may obtain a copy of the Licence at:\n\
\n\
http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available\n\
\n\
Unless required by applicable law or agreed to in\n\
writing, software distributed under the Licence is\n\
distributed on an “AS IS” basis,\n\
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either\n\
express or implied.\n\
See the Licence for the specific language governing\n\
permissions and limitations under the Licence.\n\
*/\n\
\n\
#include \"" + object()->elementName() + ".h\"\n\
\n\
" + object()->elementName() + "::" + object()->elementName() + "(Project* const Project) : Keyboard(\"" + object()->elementName() + "\", Project)\n\
{\n\
}\n\
\n\
" + object()->elementName() + "::~" + object()->elementName() + "()\n\
{\n\
}\n\
\n\
void " + object()->elementName() + "::keyPressed(int Key)\n\
{\n");

bool first=true;
for (int i=0 ; i < object()->childSignals().size() ; i++)
{
	QChar tempKey = object()->childSignals().at(i)->elementName().at(object()->childSignals().at(i)->elementName().size() - 1);
	
	if (!first) tempString += "else ";
	else first = false;
	
	tempString += "if (Key == Qt::Key_" + QString(tempKey) + ") { project()->log()->write(name(), \"Key \\\"" + QString(tempKey) + "\\\" pressed.\"); emit Key" + QString(tempKey) + "(); }\n";
}

tempString += "}\n\n";

return tempString;
}

QString MecKeyboardObjectCompiler::projectInstructions()
{
	return QString("HEADERS += Keyboard/Keyboard.h\nSOURCES += Keyboard/Keyboard.cpp\n");
}


