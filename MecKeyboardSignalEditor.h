/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#ifndef __MECKEYBOARDSIGNALEDITOR_H__
#define __MECKEYBOARDSIGNALEDITOR_H__

#include <MecSignalEditor.h>

/**
\brief	Classe d'édition d'un signal « Keyboard ».
*/
class MecKeyboardSignalEditor : public MecSignalEditor
{
	Q_OBJECT
	
	public:
	/**
	\brief	Constructeur.
	\param	Signal	Signal édité, doit absolument exister lors de la construction (c.à.d. instancié et différent de 0) sinon un comportement inattendu pourrait se produire.
	*/
	MecKeyboardSignalEditor(MecAbstractSignal* const Signal, MecAbstractEditor* MainEditor, QWidget * Parent=0, Qt::WindowFlags F=0);
	/**
	\brief	Destructeur.
	*/
	~MecKeyboardSignalEditor();
	
	///Indique si une MecVariable peut être ajouté, retourne donc ici \e false.
	bool canAddVariable() const;
	///Indique si un élément enfant peut être supprimé, retourne donc \e false.
	bool canRemoveChild(MecAbstractElement* const Element) const;
	
	public slots:
	///Ne fait rien.
	void addVariable();

};

#endif /* __MECKEYBOARDSIGNALEDITOR_H__ */

