######################################################################
# Project file implemented for qmake (version 3 with Qt 5)
######################################################################

include(../plugin/plugin.pri)
TARGET = keyboardobject
DEPENDPATH += .
INCLUDEPATH += .
RESOURCES += resources.qrc
TRANSLATIONS = translations/keyboardobject.fr.ts \
               translations/keyboardobject.de.ts

OTHER_FILES += metadata.json

HEADERS +=	    MecKeyboardObjectEditor.h \
                MecKeyboardSignalEditor.h \
                MecKeyboardObjectCompiler.h \
                MecKeyboardObjectPlugin.h
              	


SOURCES +=	    MecKeyboardObjectEditor.cpp \
                MecKeyboardSignalEditor.cpp \
                MecKeyboardObjectCompiler.cpp \
                MecKeyboardObjectPlugin.cpp
                


