/*
© Quentin VIGNAUD, 2013

Licensed under the EUPL, Version 1.1 only.
You may not use this work except in compliance with the
Licence.
You may obtain a copy of the Licence at:

http://joinup.ec.europa.eu/software/page/eupl/licence-eupl — 22 languages available

Unless required by applicable law or agreed to in
writing, software distributed under the Licence is
distributed on an “AS IS” basis,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
express or implied.
See the Licence for the specific language governing
permissions and limitations under the Licence.
*/

#include "MecKeyboardObjectEditor.h"

MecKeyboardObjectEditor::MecKeyboardObjectEditor(MecAbstractObject* const Object, MecAbstractEditor* MainEditor, QWidget * Parent, Qt::WindowFlags F) : MecObjectEditor(Object, MainEditor, Parent, F)
{
tabWidgetMain->removeTab(tabWidgetMain->indexOf(widgetFunctions));
tabWidgetMain->removeTab(tabWidgetMain->indexOf(widgetVariables));
}

MecKeyboardObjectEditor::~MecKeyboardObjectEditor()
{
}
	
bool MecKeyboardObjectEditor::canAddFunction() const
{
return false;
}

bool MecKeyboardObjectEditor::canAddVariable() const
{
return false;
}

	
MecAbstractElementEditor* MecKeyboardObjectEditor::newSubEditor(MecAbstractElement *Element)
{
if (Element->elementRole() == MecAbstractElement::Signal)
	{
	MecElementEditor *tempEditor = new MecKeyboardSignalEditor(static_cast<MecAbstractSignal*>(Element), mainEditor());
	tempEditor->childListElementsChanged(Element);
	tabWidgetSignals->addTab(tempEditor, QIcon(":/share/icons/types/" + Element->elementType() + ".png"), Element->elementName());
	return tempEditor;
	}
else return 0;
}

void MecKeyboardObjectEditor::addSignal()
{
	QString tempKeys("ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
	
	for (int i=0 ; i < object()->childSignals().size() ; i++)
	{
		QChar tempKey = object()->childSignals().at(i)->elementName().at(object()->childSignals().at(i)->elementName().size() - 1);
		tempKeys.remove(tempKey);
	}
	
	QStringList tempList;
	for (int i=0 ; i < tempKeys.size() ; i++)
	{
		tempList.append("Key" + QString(tempKeys.at(i)));
	}
	
	bool valid = false;
	QString signalName = QInputDialog::getItem(this, tr("Select signal"), tr("Select the signal to append:"), tempList, 0, false, &valid);
	if (!valid) return;
	
	MecAbstractElement *tempElement = mainEditor()->read(":/share/base/signal/Keyboard/" + signalName + ".mec");
	tempElement->setParentElement(object());
	mainEditor()->addEditStep(tr("Add the signal “%1”").arg(tempElement->elementName()));
}
	
void MecKeyboardObjectEditor::addFunction() {}
void MecKeyboardObjectEditor::addVariable() {}


